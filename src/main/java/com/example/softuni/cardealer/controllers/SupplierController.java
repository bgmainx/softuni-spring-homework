package com.example.softuni.cardealer.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.softuni.cardealer.entities.Supplier;
import com.example.softuni.cardealer.services.api.SupplierService;


@RestController
@RequestMapping("/suppliers")
public class SupplierController
{
    @Autowired
    private SupplierService supplierService;
    
    
    @GetMapping("/local")
    public List<Supplier> getLocal()
    {
        return supplierService.getLocalSuppliers();
    }
    
    @GetMapping("/importers")
    public List<Supplier> getImporters()
    {
        return supplierService.getAbroadSuppliers();
    }
}
