package com.example.softuni.cardealer.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.softuni.cardealer.entities.Car;
import com.example.softuni.cardealer.services.api.CarService;

@RestController
@RequestMapping("/cars")
public class CarController
{
    @Autowired
    private CarService carServiceImpl;
    
    @GetMapping("/all")
    public List<Car> getAll(){
        return carServiceImpl.getAll();
    }
    
    @GetMapping("{make}")
    public List<Car> getByMake(@PathVariable(name = "make") String make)
    {
        return carServiceImpl.getByMake(make);
    }
}
