package com.example.softuni.cardealer.controllers;

import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.softuni.cardealer.entities.Customer;
import com.example.softuni.cardealer.services.impl.CustomerServiceImpl;

@RestController
@RequestMapping("/customers")
public class CustomersController
{
    @Autowired
    private CustomerServiceImpl customerServiceImpl;
    
    @GetMapping("/all")
    public List<Customer> index()
    {
       return customerServiceImpl.getAll();
    }
    
    @GetMapping("/all/ascending")
    public List<Customer> getAscending() {
        return customerServiceImpl.getAscending();
    }
    
    @GetMapping("/all/descending")
    public List<Customer> getDescending() {
        return customerServiceImpl.getDescending();
    }
}
