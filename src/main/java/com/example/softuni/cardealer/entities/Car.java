package com.example.softuni.cardealer.entities;

@SuppressWarnings("unused")
public class Car
{
    private int id;
    private String make;
    private long travelledDistance;
    private String model;
    
    public Car(int id, String make, long travelledDistance, String model) 
    {
        this.id = id;
        this.make = make;
        this.travelledDistance = travelledDistance;
        this.model = model;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getMake()
    {
        return make;
    }

    public void setMake(String make)
    {
        this.make = make;
    }

    public long getTravelledDistance()
    {
        return travelledDistance;
    }

    public void setTravelledDistance(long travelledDistance)
    {
        this.travelledDistance = travelledDistance;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }
    
    public static class Builder
    {
        private int id;
        private String make;
        private long travelledDistance;
        private String model;
        
        public Builder(int id)
        {
            this.id = id;
        }
        
        public Builder withMake(String make)
        {
            this.make = make;
            
            return this;
        }
        
        public Builder withDistance(Long distance)
        {
            this.travelledDistance = distance;
            
            return this;
        }
        
        
        public Builder withModel(String model)
        {
            this.model = model;
            
            return this;
        }
        
        public Car build()
        {
            return new Car(id, make, travelledDistance, model);
        }
    }
}
