package com.example.softuni.cardealer.entities;


import java.util.Date;


@SuppressWarnings("unused")
public class Customer
{
    private int id;

    private String name;

    private boolean isYoungDriver;

    private Date birthDate;

    private Customer(int id, String name, boolean isYoungDriver, Date birthDate)
    {
        this.id = id;
        this.name = name;
        this.isYoungDriver = isYoungDriver;
        this.birthDate = birthDate;
    }
    
    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public boolean isYoungDriver()
    {
        return isYoungDriver;
    }

    public Date getBirthDate()
    {
        return birthDate;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setYoungDriver(boolean isYoungDriver)
    {
        this.isYoungDriver = isYoungDriver;
    }

    public void setBirthDate(Date birthDate)
    {
        this.birthDate = birthDate;
    }

    
    public static class Builder
    {
        private int id;

        private String name;

        private boolean isYoungDriver;

        private Date birthDate;

        public Builder(int id)
        {
            this.id = id;
        }


        public Builder withName(String name)
        {
            this.name = name;

            return this;
        }


        public Builder youngDriver(boolean isYoungDriver)
        {
            this.isYoungDriver = isYoungDriver;

            return this;
        }


        public Builder withBirthDate(Date birthDay)
        {
            this.birthDate = birthDay;

            return this;
        }


        public Customer build()
        {
            return new Customer(id, name, isYoungDriver, birthDate);
        }
    }
}
