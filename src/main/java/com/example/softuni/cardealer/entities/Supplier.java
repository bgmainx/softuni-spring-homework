package com.example.softuni.cardealer.entities;


@SuppressWarnings("unused")
public class Supplier
{
    private int id;

    private String name;

    private boolean isImporter;

    public Supplier(int id, String name, boolean isImporter)
    {
        this.id = id;
        this.name = name;
        this.isImporter = isImporter;
    }


    public int getId()
    {
        return id;
    }


    public void setId(int id)
    {
        this.id = id;
    }


    public String getName()
    {
        return name;
    }


    public void setName(String name)
    {
        this.name = name;
    }


    public boolean isImporter()
    {
        return isImporter;
    }


    public void setImporter(boolean isImporter)
    {
        this.isImporter = isImporter;
    }

    public static class Builder
    {
        private int id;

        private String name;

        private boolean isImporter;

        public Builder(int id)
        {
            this.id = id;
        }


        public Builder withName(String name)
        {
            this.name = name;

            return this;
        }


        public Builder withImporterCheck(boolean isImporter)
        {
            this.isImporter = isImporter;

            return this;
        }


        public Supplier build()
        {
            return new Supplier(id, name, isImporter);
        }
    }
}
