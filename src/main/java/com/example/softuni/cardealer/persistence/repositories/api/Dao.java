package com.example.softuni.cardealer.persistence.repositories.api;

import java.util.List;

public interface Dao<R, P>
{
    List<R> getAll();
    
}
