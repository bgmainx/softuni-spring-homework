package com.example.softuni.cardealer.persistence.repositories.api;

import java.util.List;

import com.example.softuni.cardealer.entities.Supplier;

public interface SupplierDao extends Dao<Supplier, Integer>
{
    public List<Supplier> getAbroadSuppliers();
    
    public List<Supplier> getLocalSuppliers();
}
