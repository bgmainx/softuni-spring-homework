package com.example.softuni.cardealer.persistence.repositories.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.softuni.cardealer.entities.Supplier;
import com.example.softuni.cardealer.persistence.repositories.api.SupplierDao;
import com.example.softuni.cardealer.util.mappers.SupplierRowMapper;


@Repository
public class SupplierDaoImpl implements SupplierDao
{
    @Autowired
    private JdbcTemplate template;

    private static final SupplierRowMapper MAPPER = new SupplierRowMapper();

    private static final String ID_FIELD = "id";

    private static final String NAME_FIELD = "name";

    private static final String IS_IMPORTER = "is_importer";

    private static final String SELECT_QUERY = "SELECT " + ID_FIELD + ", " + NAME_FIELD
            + ", " + IS_IMPORTER + " FROM suppliers";
    
    private static final String SELECT_ABROAD_QUERY = "SELECT " + ID_FIELD + ", " + NAME_FIELD
            + ", " + IS_IMPORTER + " FROM suppliers WHERE " + IS_IMPORTER + "= '1'";
    
    private static final String SELECT_LOCAL_QUERY = "SELECT " + ID_FIELD + ", " + NAME_FIELD
            + ", " + IS_IMPORTER + " FROM suppliers WHERE " + IS_IMPORTER + "= '0'";
   

    @Override
    public List<Supplier> getAll()
    {
        return template.query(SELECT_QUERY, MAPPER);
    }


    @Override
    public List<Supplier> getAbroadSuppliers()
    {
        
        return template.query(SELECT_ABROAD_QUERY, MAPPER);
    }


    @Override
    public List<Supplier> getLocalSuppliers()
    {
        return template.query(SELECT_LOCAL_QUERY, MAPPER);
    }

}
