package com.example.softuni.cardealer.persistence.repositories.api;


import java.util.List;


import com.example.softuni.cardealer.entities.Car;


public interface CarDao extends Dao<Car, Integer>
{
    public List<Car> getByMake(String make);
}
