package com.example.softuni.cardealer.persistence.repositories.impl;


import java.util.List;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.softuni.cardealer.entities.Customer;
import com.example.softuni.cardealer.persistence.repositories.api.CustomerDao;
import com.example.softuni.cardealer.util.mappers.CustomerRowMapper;


@Repository
public class CustomerDaoImpl implements CustomerDao
{
    private static final String ID_FIELD = "id";

    private static final String NAME_FIELD = "name";

    private static final String IS_YOUNG_DRIVER_FIELD = "is_young_driver";

    private static final String BIRTHDATE_FIELD = "birth_date";

    private static final String SELECT_QUERY = "SELECT " + ID_FIELD + ", " + NAME_FIELD + ", " + IS_YOUNG_DRIVER_FIELD
            + ", " + BIRTHDATE_FIELD + " FROM customers";

    @Autowired
    private JdbcTemplate template;

    private static final CustomerRowMapper MAPPER = new CustomerRowMapper();

    @Override
    public List<Customer> getAll()
    {
        return template.query(SELECT_QUERY, MAPPER);
    }
}
