package com.example.softuni.cardealer.persistence.repositories.api;

import com.example.softuni.cardealer.entities.Customer;

public interface CustomerDao extends Dao<Customer, Integer>
{

}
