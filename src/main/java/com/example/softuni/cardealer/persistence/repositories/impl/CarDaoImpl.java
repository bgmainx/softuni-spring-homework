package com.example.softuni.cardealer.persistence.repositories.impl;


import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.example.softuni.cardealer.entities.Car;
import com.example.softuni.cardealer.persistence.repositories.api.CarDao;
import com.example.softuni.cardealer.util.mappers.CarRowMapper;


@Repository
public class CarDaoImpl implements CarDao
{
    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final CarRowMapper MAPPER = new CarRowMapper();

    private static final String GET_ALL_QUERY = "SELECT * FROM cars";

    private static final String GET_BY_MAKE_QUERY = "SELECT * FROM cars WHERE make = ?";

    public List<Car> getAll()
    {
        return jdbcTemplate.query(GET_ALL_QUERY, MAPPER);
    }


    public List<Car> getByMake(String make)
    {
        if (make.isEmpty())
            return new ArrayList<>();

        return jdbcTemplate.query(GET_BY_MAKE_QUERY, MAPPER, make);
    }
}
