package com.example.softuni.cardealer.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Comparator;

import com.example.softuni.cardealer.entities.Car;
import com.example.softuni.cardealer.persistence.repositories.api.CarDao;
import com.example.softuni.cardealer.services.api.CarService;

@Service
public class CarServiceImpl implements CarService
{
    @Autowired
    private CarDao carDao;
    
    public List<Car> getAll()
    {
        return carDao.getAll();
    }
    
    @Override
    public List<Car> getByMake(String make)
    {
        List<Car> cars = getAll();
        
        cars.sort(Comparator.comparing(Car::getModel, (c1, c2) -> c1.compareTo(c2)
        ).thenComparing(Car::getTravelledDistance, (d1, d2) -> Long.compare(d2, d1)));
        
        return carDao.getByMake(make);
    }
}
