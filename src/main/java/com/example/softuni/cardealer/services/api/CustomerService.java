package com.example.softuni.cardealer.services.api;

import java.util.List;

import com.example.softuni.cardealer.entities.Customer;

public interface CustomerService extends Service<Customer, Integer>
{
    public List<Customer> getAscending();
    
    public List<Customer> getDescending();
}
