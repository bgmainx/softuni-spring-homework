package com.example.softuni.cardealer.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.softuni.cardealer.entities.Supplier;
import com.example.softuni.cardealer.persistence.repositories.api.SupplierDao;
import com.example.softuni.cardealer.services.api.SupplierService;

@Service
public class SupplierServiceImpl implements SupplierService
{
    @Autowired
    SupplierDao supplierDao;

    @Override
    public List<Supplier> getAbroadSuppliers()
    {
        return supplierDao.getAbroadSuppliers();
    }

    @Override
    public List<Supplier> getLocalSuppliers()
    {
        return supplierDao.getLocalSuppliers();
    }

}
