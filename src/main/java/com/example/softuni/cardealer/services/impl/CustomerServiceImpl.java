package com.example.softuni.cardealer.services.impl;


import java.util.Collections;

import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.softuni.cardealer.entities.Customer;
import com.example.softuni.cardealer.persistence.repositories.api.Dao;
import com.example.softuni.cardealer.services.api.CustomerService;


@Service
public class CustomerServiceImpl implements CustomerService
{
    @Autowired
    private Dao<Customer, ?> customerDao;

    @Override
    public List<Customer> getAll()
    {
        return customerDao.getAll();
    }


    @Override
    public List<Customer> getAscending()
    {
        List<Customer> list = getAll();

        Collections.sort(list, (c1, c2) -> c1.getBirthDate().compareTo(c2.getBirthDate()));

        return list;
    }


    @Override
    public List<Customer> getDescending()
    {
        List<Customer> list = getAll();
        // return list.stream()
        // .sorted(Comparator.comparing(Customer::getBirthDate).reversed())
        // .collect(Collectors.toList());

        list.sort(Comparator.comparing(Customer::getBirthDate).reversed());

        return list;
    }

    /*
     * one way to sort list.sort((Customer o1, Customer o2) -> o2.getBirthDate().compareTo(o1.getBirthDate()));
     * 
     * another way to sort return list.stream() .sorted((Customer o1, Customer o2) ->
     * o2.getBirthDate().compareTo(o1.getBirthDate())) .collect(Collectors.toList());
     * 
     * third way return list.stream() .sorted(Comparator.comparing(Customer::getBirthDate).reversed())
     * .collect(Collectors.toList());
     *
     * list.stream() .sorted(Comparator.comparing(Customer::getBirthDate)) .collect(Collectors.toList());
     * 
     * list.stream().sorted((Customer o1, Customer o2) -> o2.getBirthDate().compareTo(o1.getBirthDate()))
     * .collect(Collectors.toList());
     */
}
