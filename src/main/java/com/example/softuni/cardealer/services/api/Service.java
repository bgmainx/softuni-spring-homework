package com.example.softuni.cardealer.services.api;

import java.util.List;

public interface Service<T, K>
{
    public List<T> getAll();
}
