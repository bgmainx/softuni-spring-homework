package com.example.softuni.cardealer.services.api;

import java.util.List;

import com.example.softuni.cardealer.entities.Supplier;

public interface SupplierService
{
    public List<Supplier> getAbroadSuppliers();
    
    public List<Supplier> getLocalSuppliers();
}
