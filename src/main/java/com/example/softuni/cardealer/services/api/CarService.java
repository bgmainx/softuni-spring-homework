package com.example.softuni.cardealer.services.api;

import java.util.List;

import com.example.softuni.cardealer.entities.Car;

public interface CarService extends Service<Car, Integer>
{
    public List<Car> getByMake(String make);
}
