package com.example.softuni.cardealer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

@SpringBootApplication
public class CarDealerApplication {
    
	public static void main(String[] args) {
		SpringApplication.run(CarDealerApplication.class, args);
	}  
	

}
