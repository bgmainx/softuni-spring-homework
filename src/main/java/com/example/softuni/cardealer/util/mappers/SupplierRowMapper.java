package com.example.softuni.cardealer.util.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.softuni.cardealer.entities.Supplier;

public class SupplierRowMapper implements RowMapper<Supplier>
{

    @Override
    public Supplier mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Supplier.Builder(rs.getInt("id"))
                .withName(rs.getString("name"))
                .withImporterCheck(rs.getBoolean("is_importer"))
                .build();
    }
    
}
