package com.example.softuni.cardealer.util.mappers;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.softuni.cardealer.entities.Customer;


public class CustomerRowMapper implements RowMapper<Customer>
{
    @Override
    public Customer mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Customer.Builder(rs.getInt("id"))
                .withName(rs.getString("name"))
                .youngDriver(rs.getBoolean("is_young_driver"))
                .withBirthDate(rs.getDate("birth_date"))
                .build();
    }
}
