package com.example.softuni.cardealer.util.mappers;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.example.softuni.cardealer.entities.Car;

public class CarRowMapper implements RowMapper<Car>
{
    
    @Override
    public Car mapRow(ResultSet rs, int rowNum) throws SQLException
    {
        return new Car.Builder(rs.getInt("id"))
                .withDistance(rs.getLong("travelled_distance"))
                .withMake(rs.getString("make"))
                .withModel(rs.getString("model"))
                .build();
    }

}
